//
//  ResultsController.swift
//  Trivia
//
//  Created by Brandon Rodriguez on 11/11/15.
//  Copyright © 2015 Brandon Rodriguez. All rights reserved.
//

import UIKit
import MultipeerConnectivity

class ResultsController: UIViewController {
    
    private let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate;

    @IBOutlet weak var blurView: UIVisualEffectView!
    @IBOutlet weak var youRight: UILabel!
    @IBOutlet weak var youWrong: UILabel!
    @IBOutlet weak var opponentRight: UILabel!
    @IBOutlet weak var opponentWrong: UILabel!
    @IBOutlet var buttons: [UIButton]!
    
    var peerReady: Bool = false;
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        if let image = appDelegate.backgroundImage {
            
            // Set image to our views layer
            self.view.layer.contents = image.CGImage;
        }
        
        // View styling
        for view in blurView.subviews {
            view.layer.shadowOffset = CGSizeMake(2, 2)
            view.layer.shadowRadius = 2;
            view.layer.shadowOpacity = 0.25;
        }
        
        for view in buttons {
            view.layer.cornerRadius = 8;
        }
        
        updateLabels();
        
        appDelegate.multiPeer.resultsController = self;
        
    }

    override func prefersStatusBarHidden() -> Bool {
        return true;
    }
    
    @IBAction func btnAction(sender: UIButton) {
        back(sender.tag);
    }
    
    // If we press Main Menu, we'll be brought back to the start screen and disconnected from our peer, if we press replay we'll send our ready and go signals and go back to the categories.
    // If we're not connected to anybody and we hit replay we'll simply be brought back to categories and disconnected from peers just in case. Multipeer gets weird.
    func back(tag: Int) {
        if tag == 0 {
            appDelegate.multiPeer.session.disconnect();
            self.navigationController?.popToRootViewControllerAnimated(true);
        } else {
            if appDelegate.multiPeer.session.connectedPeers.count > 0 {
                if peerReady {
                    do {
                        try appDelegate.multiPeer.session.sendData("go".dataUsingEncoding(NSUTF8StringEncoding)!, toPeers: appDelegate.multiPeer.session.connectedPeers, withMode: MCSessionSendDataMode.Reliable);
                    } catch let error as NSError {
                        print("\(error)");
                    }
                    self.navigationController?.popToViewController((navigationController?.viewControllers[2])!, animated: true);
                } else {
                    do {
                        try appDelegate.multiPeer.session.sendData("ready".dataUsingEncoding(NSUTF8StringEncoding)!, toPeers: appDelegate.multiPeer.session.connectedPeers, withMode: MCSessionSendDataMode.Reliable);
                    } catch let error as NSError {
                        print("\(error)");
                    }
                }
            } else {
                self.navigationController?.popToViewController((navigationController?.viewControllers[2])!, animated: true);
                appDelegate.multiPeer.session.disconnect();
            }
        }
    }
    
    func updateLabels() {
        // Set our right and wrong label
        youRight.text = "\(appDelegate.right)"
        youWrong.text = "\(appDelegate.wrong)"
        opponentRight.text = "\(appDelegate.peerRight)"
        opponentWrong.text = "\(appDelegate.peerWrong)"
    }
    
    override func viewWillAppear(animated: Bool) {
        navigationController?.setNavigationBarHidden(true, animated: true);

    }

}
