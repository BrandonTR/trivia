//
//  MyMultipeer.swift
//  Trivia
//
//  Created by Brandon Rodriguez on 11/8/15.
//  Copyright © 2015 Brandon Rodriguez. All rights reserved.
//

import UIKit
import MultipeerConnectivity

class MyMultipeer: NSObject, MCSessionDelegate, MCBrowserViewControllerDelegate {
    
    var peerID: MCPeerID!
    var session: MCSession!
    var advertiser: MCAdvertiserAssistant!
    let serviceID = "Brandon-Trivia";
    var browser: MCBrowserViewController!
    
    private let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate;
    
    var underlyingController: ChooseController!
    var categoryController: CategoryController!
    var resultsController: ResultsController!
    
    // MARK: = MCSessionDelegate required functions
    
    // Remote peer changed state.
    func session(session: MCSession, peer peerID: MCPeerID, didChangeState state: MCSessionState) {
        dispatch_async(dispatch_get_main_queue(), {
            
            // Once we're connected to our opponent, close the multipeer browser because we no longer need it. We're only battling one person.
            switch state {
            case MCSessionState.Connected:
                if session.connectedPeers.count > 1 {
                    // Do nothing, we're not connected to anybody
                } else {
                    
                    self.browser.dismissViewControllerAnimated(true, completion: { () -> Void in
                        self.underlyingController.toCategories();
                    })
                    self.advertiser.stop();
                    
                }
                break;
            case MCSessionState.Connecting:
                break;
            case MCSessionState.NotConnected:
                break;
            }
        })
        
    }
    
    // Received data from remote peer.
    func session(session: MCSession, didReceiveData data: NSData, fromPeer peerID: MCPeerID) {
        // This is where we handle our peers data, if they got something wrong, that's what they'll broadcast and vica versa. We also handle the replay
        // stuff here. If we did not give the go signal then we signal we're ready and once the other person presses replay you'll both be brought back
        // to the categories controller.
        // If we receive something other than these strings, then we're receiving the ID. That's how we tell each other which ID we've chosen.
        if data.isEqualToData("right".dataUsingEncoding(NSUTF8StringEncoding)!) {
            appDelegate.peerRight = ++appDelegate.peerRight;
            if let finalController = resultsController {
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    finalController.updateLabels();
                })
            }
        } else if data.isEqualToData("wrong".dataUsingEncoding(NSUTF8StringEncoding)!) {
            appDelegate.peerWrong = ++appDelegate.peerWrong;
            if let finalController = resultsController {
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    finalController.updateLabels();
                })
            }
        } else if data.isEqualToData("ready".dataUsingEncoding(NSUTF8StringEncoding)!) {
            if let finalController = resultsController {
                finalController.peerReady = true;
            }
        } else if data.isEqualToData("go".dataUsingEncoding(NSUTF8StringEncoding)!) {
            if let finalController = resultsController {
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    finalController.navigationController?.popToViewController((finalController.navigationController?.viewControllers[2])!, animated: true);
                })
            }
        } else {
            let stringID: String = String.init(data: data, encoding: NSUTF8StringEncoding)!;
            let id: Int = Int(stringID)!
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                self.categoryController.toQuestions(id);
            })
        }
    }
    
    // Received a byte stream from remote peer.
    func session(session: MCSession, didReceiveStream stream: NSInputStream, withName streamName: String, fromPeer peerID: MCPeerID) {
        
    }
    
    func session(session: MCSession, didStartReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, withProgress progress: NSProgress) {
        
    }
    
    // Finished receiving a resource from remote peer and saved the content
    // in a temporary location - the app is responsible for moving the file
    // to a permanent location within its sandbox.
    func session(session: MCSession, didFinishReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, atURL localURL: NSURL, withError error: NSError?) {
        
    }
    
    override init() {
        super.init();
        
        // MCSessionDelegate
        self.peerID = MCPeerID(displayName: UIDevice.currentDevice().name);
        self.session = MCSession(peer: self.peerID);
        self.session.delegate = self;
        self.advertiser = MCAdvertiserAssistant(serviceType: self.serviceID, discoveryInfo: nil, session: self.session);
        
        // MCBrowserViewControllerDelegate
        browser = MCBrowserViewController(serviceType: serviceID, session: session);
        browser.maximumNumberOfPeers = 1;
        browser.delegate = self;
    }
    
    // MARK: - MCBrowserViewControllerDelegate required functions
    
    // Notifies the delegate, when the user taps the done button.
    func browserViewControllerDidFinish(browserViewController: MCBrowserViewController) {
        browserViewController.dismissViewControllerAnimated(true, completion: nil);
    }
    
    // Notifies delegate that the user taps the cancel button.
    func browserViewControllerWasCancelled(browserViewController: MCBrowserViewController) {
        browserViewController.dismissViewControllerAnimated(true, completion: nil);
    }

}
