//
//  QuestionsController.swift
//  Trivia
//
//  Created by Brandon Rodriguez on 11/11/15.
//  Copyright © 2015 Brandon Rodriguez. All rights reserved.
//

import UIKit
import MultipeerConnectivity

class QuestionsController: UIViewController, UITextFieldDelegate {
    
    private let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate;
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var blurView: UIVisualEffectView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var countdownLabel: UILabel!
    @IBOutlet weak var questionLabel: UILabel!
    @IBOutlet weak var answerField: UITextField!
    
    var timer: NSTimer = NSTimer();
    var count: Int = 10;
    
    let alertController: UIAlertController = UIAlertController(title: "Forfeit?", message: "Are you sure you want to forfeit? You will be disconnected from your peer(if applicable) and brought back to the main menu.", preferredStyle: UIAlertControllerStyle.Alert);
    var yesAction: UIAlertAction!
    var noAction: UIAlertAction!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        if let image = appDelegate.backgroundImage {
            
            // Set image to our views layer
            self.view.layer.contents = image.CGImage;
        }
        
        // View styling
        for view in blurView.subviews {
            view.layer.shadowOffset = CGSizeMake(2, 2)
            view.layer.shadowRadius = 2;
            view.layer.shadowOpacity = 0.25;
        }
        
        yesAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.Default, handler: {action in
            self.forfeit();
        })
        noAction = UIAlertAction(title: "No", style: UIAlertActionStyle.Destructive, handler: {action in
            self.alertController.dismissViewControllerAnimated(true, completion: nil);
        })
        
        alertController.addAction(yesAction);
        alertController.addAction(noAction);

        questionLabel.text = appDelegate.clueArray[0][0]
        
        // Let's make sure that keyboard doesn't get in the way
        answerField.delegate = self;
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardWillShow:", name:UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardWillHide:", name:UIKeyboardWillHideNotification, object: nil)

    }

    override func prefersStatusBarHidden() -> Bool {
        return true;
    }
    
    override func viewWillAppear(animated: Bool) {
        navigationController?.setNavigationBarHidden(true, animated: true);
        timer = NSTimer.scheduledTimerWithTimeInterval(1, target:self, selector: Selector("decrementCount"), userInfo: nil, repeats: true);
    }
    
    @IBAction func forfeitAction(sender: UIButton) {
        self.presentViewController(alertController, animated: true, completion: nil);
    }
    
    // If answer is blank, WRONG! If answer is more than 3 characters off from answer from API, WRONG! If not, you're good to go.
    func checkAnswer() {
        if answerField.text == "" {
            appDelegate.wrong = ++appDelegate.wrong;
            if appDelegate.multiPeer.session.connectedPeers.count > 0 {
                do {
                    try appDelegate.multiPeer.session.sendData("wrong".dataUsingEncoding(NSUTF8StringEncoding)!, toPeers: appDelegate.multiPeer.session.connectedPeers, withMode: MCSessionSendDataMode.Reliable);
                } catch let error as NSError {
                    print("\(error)");
                }
            }
        } else {
            if Tools.levenshtein(answerField.text!, bStr: appDelegate.clueArray[1][0]!) < 3 {
                appDelegate.right = ++appDelegate.right;
                if appDelegate.multiPeer.session.connectedPeers.count > 0 {
                    do {
                        try appDelegate.multiPeer.session.sendData("right".dataUsingEncoding(NSUTF8StringEncoding)!, toPeers: appDelegate.multiPeer.session.connectedPeers, withMode: MCSessionSendDataMode.Reliable);
                    } catch let error as NSError {
                        print("\(error)");
                    }
                }
            } else {
                appDelegate.wrong = ++appDelegate.wrong;
                if appDelegate.multiPeer.session.connectedPeers.count > 0 {
                    do {
                        try appDelegate.multiPeer.session.sendData("wrong".dataUsingEncoding(NSUTF8StringEncoding)!, toPeers: appDelegate.multiPeer.session.connectedPeers, withMode: MCSessionSendDataMode.Reliable);
                    } catch let error as NSError {
                        print("\(error)");
                    }
                }
            }
        }
        moveOn();
    }
    
    func forfeit() {
        appDelegate.multiPeer.session.disconnect();
        self.navigationController?.popToRootViewControllerAnimated(true);
    }
    
    func decrementCount () {
        countdownLabel.text = String(--count);
        
        // Once we've reached 0, stop the timer, and check our answer.
        if count == 0 {
            checkAnswer();
        }
    }
    
    // Once we've answered a question, remove it from our array then continue to the next question. If we've ran out of questions then go to our results.
    func moveOn() {
        timer.invalidate();
        if appDelegate.clueArray[0].count > 1 {
            appDelegate.clueArray[0].removeAtIndex(0);
            appDelegate.clueArray[1].removeAtIndex(0);
            let nextQuestion: QuestionsController = storyboard?.instantiateViewControllerWithIdentifier("QuestionsController") as! QuestionsController;
            self.navigationController?.pushViewController(nextQuestion, animated: true);
        } else {
            self.performSegueWithIdentifier("toResults", sender: self);
        }
    }
    
    // MARK: - UITextField Methods
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder();
        checkAnswer();
        return true;
    }
    
    func keyboardWillShow(notification:NSNotification){
        
        var userInfo = notification.userInfo!
        var keyboardFrame:CGRect = (userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue).CGRectValue();
        keyboardFrame = self.view.convertRect(keyboardFrame, fromView: nil);
        
        var contentInset:UIEdgeInsets = self.scrollView.contentInset;
        contentInset.bottom = keyboardFrame.size.height + 8;
        self.scrollView.contentInset = contentInset;
    }
    
    func keyboardWillHide(notification:NSNotification){
        
        let contentInset:UIEdgeInsets = UIEdgeInsetsZero;
        self.scrollView.contentInset = contentInset;
    }

}
