//
//  TriviaAPI.swift
//  Trivia
//
//  Created by Brandon Rodriguez on 11/13/15.
//  Copyright © 2015 Brandon Rodriguez. All rights reserved.
//

import UIKit

class TriviaAPI {
    
    // Function to pull a certain amount of categories from our API and return the arrays.
    func categories(count: Int) -> [[AnyObject]] {
        let categoryURL: NSURL? = NSURL(string: "http://jservice.io/api/categories?count=\(count)");
        let categoryData: NSData? = NSData(contentsOfURL: categoryURL!);
        var categoryTitle: String?
        var categoryID: Int?
        var categoryArray: [String] = [];
        var categoryIdentifiers : [Int] = [];
        var json: JSON!
        
        if let data = categoryData {
            json = JSON(data: data);
            
            // Appending JSON data to category array
            for category in json.array! {
                categoryTitle = category["title"].string;
                categoryID = category["id"].int;
                
                // String formatting
                categoryTitle!.replaceRange(categoryTitle!.startIndex...categoryTitle!.startIndex, with: String(categoryTitle![categoryTitle!.startIndex]).capitalizedString)
                
                categoryArray.append(categoryTitle!);
                categoryIdentifiers.append(categoryID!);
            }
        }
        
        return [categoryArray, categoryIdentifiers];
    }
    
    // Function to get the questions from our API with the passed in ID that is stored in our cells.
    func questions(id: Int) -> [[String?]] {
        let questionsURL: NSURL? = NSURL(string: "http://jservice.io/api/clues?category=\(id)");
        let questionsData: NSData? = NSData(contentsOfURL: questionsURL!);
        var question: String?
        var answer: String?
        var questionArray: [String?] = [];
        var answerArray: [String?] = [];
        var json: JSON!
        
        if let data = questionsData {
            json = JSON(data: data);
            
            // Appending JSON data to category array
            for clue in json.array! {
                question = clue["question"].string!.stringByReplacingOccurrencesOfString("<[^>]+>", withString: "", options: .RegularExpressionSearch, range: nil);
                answer = clue["answer"].string!.stringByReplacingOccurrencesOfString("<[^>]+>", withString: "", options: .RegularExpressionSearch, range: nil);
                
                questionArray.append(question);
                answerArray.append(answer);
            }
        }
        
        return [questionArray, answerArray];
    }
}