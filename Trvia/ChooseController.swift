//
//  ChooseController.swift
//  Trivia
//
//  Created by Brandon Rodriguez on 11/4/15.
//  Copyright © 2015 Brandon Rodriguez. All rights reserved.
//

import UIKit
import MultipeerConnectivity

class ChooseController: UIViewController {
    
    private let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate;
    
    @IBOutlet weak var blurView: UIVisualEffectView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Setting the image behind our blurView
        if let image = appDelegate.backgroundImage {
            
            // Set image to our views layer
            self.view.layer.contents = image.CGImage;
        }
        
        // View styling
        for view in blurView.subviews {
            view.layer.shadowOffset = CGSizeMake(2, 2)
            view.layer.shadowRadius = 2;
            view.layer.shadowOpacity = 0.25;
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        navigationController?.setNavigationBarHidden(true, animated: true);
        appDelegate.resetProperties();
    }
    
    override func prefersStatusBarHidden() -> Bool {
        return true;
    }
    
    func toCategories() {
        self.performSegueWithIdentifier("toCategories", sender: self);
    }


    @IBAction func soloPressed(sender: UIButton) {
    }
    
    // Start advertising we wanna play some multiplayer and show us our peers!
    @IBAction func multiPressed(sender: UIButton) {
        appDelegate.multiPeer.advertiser.start();
        appDelegate.multiPeer.underlyingController = self;
        self.presentViewController(appDelegate.multiPeer.browser, animated: true, completion: nil);
    }
    
}
