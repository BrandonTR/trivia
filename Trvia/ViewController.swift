//
//  ViewController.swift
//  Trivia
//
//  Created by Brandon Rodriguez on 11/3/15.
//  Copyright © 2015 Brandon Rodriguez. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    private let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate;

    @IBOutlet weak var playBtn: UIButton!
    @IBOutlet weak var blurView: UIVisualEffectView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Setting the image behind our blurView
        if let image = appDelegate.backgroundImage {
            
            // Set image to our views layer
            self.view.layer.contents = image.CGImage;
        }
        
        // View styling
        for view in blurView.subviews {
            view.layer.shadowOffset = CGSizeMake(2, 2)
            view.layer.shadowRadius = 2;
            view.layer.shadowOpacity = 0.25;
        }
        
        // Navigation Controller styling
        navigationController?.navigationBar.setBackgroundImage(UIImage(), forBarMetrics: UIBarMetrics.Default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.translucent = true;
        navigationController?.navigationBar.tintColor = UIColor.whiteColor();
        
        playBtn.layer.cornerRadius = (playBtn.frame.width / 2);
        
    }
    
    override func viewWillAppear(animated: Bool) {
        navigationController?.setNavigationBarHidden(true, animated: true);
        // If we're at this screen, all our properties should be where the started.
        appDelegate.resetProperties();
    }

    override func prefersStatusBarHidden() -> Bool {
        return true;
    }
    
    @IBAction func playPressed(sender: UIButton) {
    }

}

