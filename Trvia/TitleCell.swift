//
//  TitleCell.swift
//  Trivia
//
//  Created by Brandon Rodriguez on 11/6/15.
//  Copyright © 2015 Brandon Rodriguez. All rights reserved.
//

import UIKit

class TitleCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.backgroundColor = UIColor.clearColor();
        
        titleLabel.layer.shadowOffset = CGSizeMake(2, 2)
        titleLabel.layer.shadowRadius = 2;
        titleLabel.layer.shadowOpacity = 0.25;
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
