//
//  CategoryController.swift
//  Trivia
//
//  Created by Brandon Rodriguez on 11/4/15.
//  Copyright © 2015 Brandon Rodriguez. All rights reserved.
//

import UIKit
import MultipeerConnectivity

class CategoryController: UITableViewController {
    
    private let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate;

    let blurView: UIVisualEffectView = UIVisualEffectView(effect: UIBlurEffect(style: UIBlurEffectStyle.Light));
    
    let triviaAPI: TriviaAPI = TriviaAPI();
    let amountOfCategories: Int = 40;
    
    var tableArray: [[String]] = [["Title Cell"]]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if let image = appDelegate.backgroundImage {
            
            // Set image to our views layer
            self.view.layer.contents = image.CGImage;
        }
        
        self.tableView.backgroundView = blurView;
        
        let categoryArray: [String] = triviaAPI.categories(amountOfCategories)[0] as! [String];
        
        tableArray.append(categoryArray);
        
        appDelegate.multiPeer.categoryController = self;
        
        self.clearsSelectionOnViewWillAppear = true;
    }

    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(true);
        appDelegate.multiPeer.session.disconnect();
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return tableArray.count;
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableArray[section].count;
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 60;
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var cell: UITableViewCell?
        if indexPath.section == 0 {
            cell = tableView.dequeueReusableCellWithIdentifier("TitleCell", forIndexPath: indexPath);
        } else {
            cell = tableView.dequeueReusableCellWithIdentifier("CategoryCell", forIndexPath: indexPath);
            let cellCasted = cell as? CategoryCell;
            let categoryArray = tableArray[indexPath.section];
            let categoryIdentifiers: [Int] = triviaAPI.categories(amountOfCategories)[1] as! [Int];

            cellCasted?.setProperties(categoryArray[indexPath.row], id: categoryIdentifiers[indexPath.row]);
        }
        
        return cell!
    }
    
    // When we select a category, if we're connected to somebody send that ID and move ourselves forward.
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let selectedCell: CategoryCell = tableView.cellForRowAtIndexPath(indexPath) as! CategoryCell;
        toQuestions(selectedCell.categoryID!);
        if appDelegate.multiPeer.session.connectedPeers.count > 0 {
            do {
                try appDelegate.multiPeer.session.sendData("\(selectedCell.categoryID!)".dataUsingEncoding(NSUTF8StringEncoding)!, toPeers: appDelegate.multiPeer.session.connectedPeers, withMode: MCSessionSendDataMode.Reliable);
            } catch let error as NSError {
                print("\(error)");
            }
        }
    
    }
    
    override func prefersStatusBarHidden() -> Bool {
        return true;
    }
    
    override func viewWillAppear(animated: Bool) {
        navigationController?.setNavigationBarHidden(false, animated: true);
    }
    
    // Once a peer selects a category, this method is called to continue us to the same questions.
    func toQuestions(id: Int) {
        appDelegate.clueArray.appendContentsOf(triviaAPI.questions(id));
        self.performSegueWithIdentifier("toQuestions", sender: self);
    }

}
