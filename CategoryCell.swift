//
//  CategoryCell.swift
//  Trivia
//
//  Created by Brandon Rodriguez on 11/6/15.
//  Copyright © 2015 Brandon Rodriguez. All rights reserved.
//

import UIKit

class CategoryCell: UITableViewCell {

    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var labelContainer: UIView!
    
    var categoryID: Int?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        labelContainer.layer.cornerRadius = 8;
        labelContainer.layer.shadowOffset = CGSizeMake(2, 2)
        labelContainer.layer.shadowRadius = 2;
        labelContainer.layer.shadowOpacity = 0.25;
        
        self.backgroundColor = UIColor.clearColor();
        
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setProperties(title: String, id: Int) {
        categoryLabel.text = title;
        categoryID = id;
    }

}
